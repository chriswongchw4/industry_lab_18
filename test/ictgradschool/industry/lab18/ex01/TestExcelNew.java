package ictgradschool.industry.lab18.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO Write tests
 */
public class TestExcelNew {

    ExcelNew excelNew;

    @Before
    public void setUp() {
        excelNew = new ExcelNew();
    }

    @Test
    public void testGenerateClassID() {
        assertEquals(excelNew.generateStudentID(5), "0005");
        assertEquals(excelNew.generateStudentID(50), "0050");
        assertEquals(excelNew.generateStudentID(500), "0500");
    }

    @Test
    public void testReadDocument() {
        String fileName = "FirstNames.txt";
        assertNotNull(excelNew.readDocument(fileName));
        String fileName2 = "Surnames.txt";
        assertNotNull(excelNew.readDocument(fileName2));
    }

    @Test
    public void testGenerateStudentName() {
        assertFalse(excelNew.generateStudentName(excelNew.readDocument("Surnames.txt")) == null);
        assertFalse(excelNew.generateStudentName(excelNew.readDocument("FirstNames.txt")) == null);
    }

    @Test
    public void testGenerateSkillLevel() {
        assertTrue(excelNew.generateSkillLevel() >= 0);
        assertTrue(excelNew.generateSkillLevel() <= 100);
    }

    @Test
    public void testGenerateLabMarks() {
        assertTrue(excelNew.generateLabMarks(5) <= 39);
        assertTrue(excelNew.generateLabMarks(65) <= 89);
        assertTrue(excelNew.generateLabMarks(65) >= 70);
    }

    @Test
    public void testGenerateTestMarks() {
        assertTrue(excelNew.generateTestMarks(40) <= 69);
        assertTrue(excelNew.generateTestMarks(40) >= 50);
    }

    @Test
    public void testGenerateExamMarks() {
        assertTrue(excelNew.generateExamMarks(91) <= 100);
        assertTrue(excelNew.generateExamMarks(91) >= 90);
    }

}