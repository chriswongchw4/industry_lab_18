package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.ArrayList;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {

    private String line;

    private static int classSize = 550;
    private static int numLabs = 3;


    public String generateStudentID(int i) {
        String studentID = "";

        if (i / 10 < 1) {
            studentID += "000" + i;
        } else if (i / 100 < 1) {
            studentID += "00" + i;
        } else if (i / 1000 < 1) {
            studentID += "0" + i;
        } else {
            studentID += i;
        }
        return studentID;
    }

    public ArrayList<String> readDocument(String fileName) {
        ArrayList<String> List = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            while ((line = br.readLine()) != null) {
                List.add(line); //make it a method
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return List;

    }

    public String generateStudentName(ArrayList<String> nameList) {
        int randIndex = (int) (Math.random() * nameList.size());
        return nameList.get(randIndex);
    }

    public int generateSkillLevel() {
        return (int) (Math.random() * 101);
    }

    public int generateLabMarks(int randStudentSkill) {
        int labMark;
        if (randStudentSkill <= 5) {
            labMark = (int) (Math.random() * 40); //[0,39] //generateLabMarks
        } else if ((randStudentSkill > 5) && (randStudentSkill <= 15)) {
            labMark = ((int) (Math.random() * 10) + 40); // [40,49]
        } else if ((randStudentSkill > 15) && (randStudentSkill <= 25)) {
            labMark = ((int) (Math.random() * 20) + 50); // [50,69]
        } else if ((randStudentSkill > 25) && (randStudentSkill <= 65)) {
            labMark = ((int) (Math.random() * 20) + 70); // [70,89]
        } else {
            labMark = ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return labMark;
    }


    public int generateTestMarks(int randStudentSkill) {

        int testMark;
        if (randStudentSkill <= 5) {
            testMark = (int) (Math.random() * 40); //[0,39] //generateTestMarks
        } else if ((randStudentSkill > 5) && (randStudentSkill <= 20)) {
            testMark = ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 65)) {
            testMark = ((int) (Math.random() * 20) + 50); //[50,69]
        } else if ((randStudentSkill > 65) && (randStudentSkill <= 90)) {
            testMark = ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            testMark = ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return testMark;
    }

    public int generateExamMarks(int randStudentSkill) {
        int examMark;
        if (randStudentSkill <= 7) { //generateExamMarks
            int randDNSProb = generateSkillLevel();
            if (randDNSProb <= 5) {
                examMark = -1; //DNS
            } else {
                examMark = (int) (Math.random() * 40); //[0,39]
            }
        } else if ((randStudentSkill > 7) && (randStudentSkill <= 20)) {
            examMark = ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 60)) {
            examMark = ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((randStudentSkill > 60) && (randStudentSkill <= 90)) {
            examMark = ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            examMark = ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return examMark;
    }


    public static void main(String[] args) {

        String output = ""; //this is the final output.

        ExcelNew excelNew = new ExcelNew();

        ArrayList<String> firstNameList = excelNew.readDocument("FirstNames.txt");
        ArrayList<String> surnameList = excelNew.readDocument("Surnames.txt");


        for (int i = 1; i <= classSize; i++) {

            output += excelNew.generateStudentID(i) + "\t";

            output += excelNew.generateStudentName(surnameList) + "\t" + excelNew.generateStudentName(firstNameList) + "\t";

            int randStudentSkill = excelNew.generateSkillLevel();

            //add lab marks
            for (int j = 0; j < numLabs; j++) {
                output += excelNew.generateLabMarks(randStudentSkill) + "\t";
            }

            //add test mark
            output += excelNew.generateTestMarks(randStudentSkill) + "\t";
            //Exam////////////
            int examMarks = excelNew.generateExamMarks(randStudentSkill);
            if (examMarks == -1) {
                output += "" + "\t";
            } else {
                output += examMarks;
            }
            //////////////////////////////////
            output += "\n";
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("Data_Out2.txt"));
            bw.write(output);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

